from django.contrib import admin
from .models import PlantCategory,PlantInfo,PlantStock

# Register your models here.
admin.site.register(PlantCategory),
admin.site.register(PlantInfo),
admin.site.register(PlantStock)