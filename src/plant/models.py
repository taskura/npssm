from django.db import models

# Create your models here.

class PlantCategory(models.Model):
	name 		   = models.CharField(max_length=100)
	
	def __str__(self):
		return self.name


class PlantInfo(models.Model):
	name 		   = models.CharField(max_length=100)
	category 	   = models.ForeignKey(PlantCategory,on_delete=models.CASCADE)
	age 		   = models.CharField(max_length=100)
	
	def __str__(self):
		return self.name

class PlantStock(models.Model):
	plant_name = models.ForeignKey(PlantInfo,on_delete=models.CASCADE)
	plant_quantity = models.PositiveIntegerField(default=0)

	def __str__(self):
		return str(self.id)