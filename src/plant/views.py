from django.shortcuts import render,redirect
from .models import PlantCategory,PlantInfo,PlantStock
from django.views.generic import ListView,CreateView,UpdateView
from .forms import PlantInfoForm,PlantCatForm,PlantStockForm
from django.http import JsonResponse
from django.template.loader import render_to_string




# Create your views here.

class PlantList(ListView):
	model = PlantInfo
	template_name = 'plant/plant_list.html'
	context_object_name = 'plant_list'
	ordering = "-pk"
	#plant_list =  PlantInfo.objects.all()

	def get_context_data(self,**kwargs):
		data = super(PlantList,self).get_context_data(**kwargs)
		data["plant_form"] = PlantInfoForm()
		return data

class PlantCreate(CreateView):
	model = PlantInfo
	template_name = 'plant/plant_create.html'
	form_class = PlantInfoForm
	success_url = '/plant/list/'

	def form_valid(self,form,*args,**kwargs):
		self.object = form.save()
		PlantStock.objects.create(plant_name=self.object)
		return super(PlantCreate,self).form_valid(form)


def delete_plant(request,id):
    plant = PlantInfo.objects.get(id=id)
    plant.delete()
    return redirect("/plant/list/")



class PlantCategoryList(ListView):
	model = PlantCategory
	template_name = 'plant/plant_cat.html'
	context_object_name = 'category_list'
	ordering = "-pk"

	def get_context_data(self,**kwargs):
		data = super(PlantCategoryList,self).get_context_data(**kwargs)
		data["category_form"] = PlantCatForm()
		return data

class PlantCatCreate(CreateView):
	model = PlantCategory
	template_name = 'plant/catergory_create.html'
	form_class = PlantCatForm
	success_url = '/plant/category/'


def delete_plant_category(request,id):
    plant = PlantCategory.objects.get(id=id)
    plant.delete()
    return redirect("/plant/category/")

class PlantStockList(ListView):
	model = PlantStock
	template_name = 'plant/plant_stock.html'
	context_object_name = 'plant_stock'
	ordering = "-pk"


def load_plant_update_form(request,id):
	data = dict()
	plant_obj = PlantInfo.objects.get(id=id)
	form = PlantInfoForm(instance=plant_obj)
	context = {
		'plant_form':form
	}
	update_html = render_to_string('plant/plant_updateform.html',context,request=request)

	data['plant_form'] = update_html
	return JsonResponse(data)

def update_plant(request,id):
	plant_obj = PlantInfo.objects.get(id=id)
	
	if request.method == 'POST':
		form = PlantInfoForm(request.POST,instance=plant_obj)
		if form.is_valid():
			form.save()

	return redirect("/plant/list/")


def update_plantstock_loadform(request,id):
	data = dict()
	plantstock_obj = PlantStock.objects.get(id=id)
	form = PlantStockForm(instance=plantstock_obj)
	context = {
		'stock_form':form
	}
	update_html = render_to_string('plant/plantstock_form.html',context,request=request)
	data['stock_form'] = update_html
	return JsonResponse(data)

def update_plantstock(request,id):
	plantstock_obj = PlantStock.objects.get(id=id)

	if request.method == 'POST':
		form = PlantStockForm(request.POST,instance=plantstock_obj)
		if form.is_valid():
			form.save()
	return redirect("/plant/stock/list/")