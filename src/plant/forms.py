from django.forms import ModelForm, inlineformset_factory, TextInput
from .models import PlantCategory,PlantInfo,PlantStock


class PlantInfoForm(ModelForm):
	class Meta:
		model = PlantInfo
		fields = '__all__'


class PlantCatForm(ModelForm):
	class Meta:
		model = PlantCategory
		fields = '__all__'

class PlantStockForm(ModelForm):
	class Meta:
		model = PlantStock
		fields = '__all__'