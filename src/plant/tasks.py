from __future__ import absolute_import, unicode_literals
from celery.decorators import task
from django.utils import timezone
from datetime import datetime, timedelta
from maintenance.models import Sheduling

from twilio.rest import Client
from django.core.mail import send_mail


@task(name="Send_Message")
def send_notification():
	all_sheduling = Sheduling.objects.all()
	for sheduling in all_sheduling:
		massage = "Today you need to spray {} into {}".format(sheduling.pesticide.name, sheduling.plant.name)
		today = datetime.now().date()

		sy, sm, sd = str(sheduling.timestemp).split('-')
		format_sheduling_date = datetime(int(sy), int(sm), int(sd))

		sheduling_date = format_sheduling_date + timedelta(days=int(sheduling.schedule_days))

		if str(today) == str(sheduling_date)[:10]:
			account_sid = "AC7ec106fbc0d05f6cea806c416e1ce192"
			auth_token = "c62bf62bc788dcda4257f45bcda597cc"
			client = Client(account_sid, auth_token)
			client.messages.create(  
				to="+8801863714903",  
				from_="+15129942395",  
				body=massage,  
				)

			send_mail(
				    'Nursery',
				    massage,
				    'info@nursery.com',
				    ['admin@nursery.com'],
				)

			sheduling.timestemp = str(today)
			sheduling.save()



	return "Done"


# celery -A npssm beat -l info -S django
# celery -A npssm worker -l info
# redis-server