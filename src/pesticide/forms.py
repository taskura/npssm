from django.forms import ModelForm, inlineformset_factory, TextInput
from .models import PesticideCategory,pesticideInfo,PesticideStock


class PesticidenfoForm(ModelForm):
	class Meta:
		model = pesticideInfo
		fields = '__all__'


class PesticideCatForm(ModelForm):
	class Meta:
		model = PesticideCategory
		fields = '__all__'

class PesticideStockForm(ModelForm):
	class Meta:
		model = PesticideStock
		fields = '__all__'