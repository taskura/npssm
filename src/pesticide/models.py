from django.db import models

# Create your models here.

class PesticideCategory(models.Model):
	name 		   = models.CharField(max_length=100)
	
	def __str__(self):
		return self.name


class pesticideInfo(models.Model):
	name 		   = models.CharField(max_length=100)
	category 	   = models.ForeignKey(PesticideCategory,on_delete=models.CASCADE)
	
	def __str__(self):
		return self.name

class PesticideStock(models.Model):
	pesticide_name = models.ForeignKey(pesticideInfo,on_delete=models.CASCADE)
	pesticide_quantity = models.PositiveIntegerField(default=0)

	def __str__(self):
		return str(self.id)