from django.shortcuts import render,redirect
from .models import PesticideCategory,pesticideInfo,PesticideStock
from django.views.generic import ListView,CreateView,UpdateView
from .forms import PesticidenfoForm,PesticideCatForm,PesticideStockForm
from django.http import JsonResponse
from django.template.loader import render_to_string



# Create your views here.

class PesticideList(ListView):
	model = pesticideInfo
	template_name = 'pesticide/pesticide_list.html'
	context_object_name = 'pesticide_list'
	ordering = "-pk"

	def get_context_data(self,**kwargs):
		data = super(PesticideList,self).get_context_data(**kwargs)
		data["pesticide_form"] = PesticidenfoForm()
		return data
class PesticideCreate(CreateView):
	model = pesticideInfo
	template_name = 'pesticide/pesticide_create.html'
	form_class = PesticidenfoForm
	success_url = '/pesticide/list/'

	def form_valid(self,form,*args,**kwargs):
		self.object = form.save()
		PesticideStock.objects.create(pesticide_name=self.object)
		return super(PesticideCreate,self).form_valid(form)

def delete_pesticide(request,id):
    pesticide = pesticideInfo.objects.get(id=id)
    pesticide.delete()
    return redirect("/pesticide/list/")


class PesticideCategoryList(ListView):
	model = PesticideCategory
	template_name = 'pesticide/category_list.html'
	context_object_name = 'category_list'
	ordering = "-pk"


	def get_context_data(self,**kwargs):
		data = super(PesticideCategoryList,self).get_context_data(**kwargs)
		data["category_form"] = PesticideCatForm()
		return data

class PesticideCatCreate(CreateView):
	model = PesticideCategory
	template_name = 'pesticide/category_create.html'
	form_class = PesticideCatForm
	success_url = '/pesticide/category/'



def delete_pesticide_category(request,id):
    pesticide = PesticideCategory.objects.get(id=id)
    pesticide.delete()
    return redirect("/pesticide/category/")

class PesticideStockList(ListView):
	model = PesticideStock
	template_name = 'pesticide/stock_list.html'
	context_object_name = 'stock_list'
	ordering = "-pk"

def load_pesticide_update_form(request,id):
	data = dict()
	pesticide_obj = pesticideInfo.objects.get(id=id)
	form = PesticidenfoForm(instance=pesticide_obj)
	context = {
		"pesticide_form":form
	}

	update_html = render_to_string('pesticide/pesticide_updateform.html',context, request=request)
	data["pesticide_form"] = update_html
	return JsonResponse(data)


def update_pesticide(request,id):
	pesticide_obj = pesticideInfo.objects.get(id=id)

	if request.method == 'POST':
		form = PesticidenfoForm(request.POST,instance=pesticide_obj)
		if form.is_valid():
			form.save()
	return redirect('/pesticide/list/')



def load_pesticidestock_form(request,id):
	data = dict()
	pesticidestock_obj = PesticideStock.objects.get(id=id)
	form = PesticideStockForm(instance = pesticidestock_obj)

	context = {
		'stock_form':form
	}

	update_html = render_to_string('pesticide/update_pesticidestock.html',context,request=request)
	data['stock_form'] = update_html

	return JsonResponse(data)


def update_pesticidestock(request,id):
	pesticidestock_obj = PesticideStock.objects.get(id=id)

	if request.method == 'POST':
		form = PesticideStockForm(request.POST, instance = pesticidestock_obj)
		if form.is_valid():
			form.save()
	return redirect('/pesticide/stock/list/')
