# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-03-16 18:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PesticideCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='pesticideInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pesticide.PesticideCategory')),
            ],
        ),
        migrations.CreateModel(
            name='PesticideStock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pesticide_quantity', models.PositiveIntegerField(default=0)),
                ('pesticide_name', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pesticide.pesticideInfo')),
            ],
        ),
    ]
