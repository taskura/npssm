"""npssm URL Configuration
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from maintenance.views import (home,MaintenanceList,MaintenanceCreate,
    report,SchedulingList,ScheduleCreate,delete_schedule,schedule_updateload_form,schedule_update)

from plant.views import (PlantList,PlantCreate,delete_plant,PlantCategoryList,
    PlantCatCreate,delete_plant_category,load_plant_update_form,PlantStockList,
    update_plant,update_plantstock_loadform,update_plantstock)

from pesticide.views import (PesticideList,PesticideCreate,delete_pesticide,
    PesticideCategoryList,PesticideCatCreate,delete_pesticide_category,
    PesticideStockList,load_pesticide_update_form,update_pesticide,
    load_pesticidestock_form,update_pesticidestock)




urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', home, name = 'home'),
    # plant urls
    url(r'^plant/list/$', PlantList.as_view(), name = 'plant_list'),
    url(r'^plant/create/$', PlantCreate.as_view(), name = 'plant_create'),
    url(r'^plant/delete/(?P<id>\d+)/$', delete_plant, name = 'plant_delete'),
    url(r'^plant/category/$', PlantCategoryList.as_view(), name = 'plant_cat_list'),
    url(r'^plant/category/create/$', PlantCatCreate.as_view(), name = 'plant_cat_create'),
    url(r'^plant/category/delete/(?P<id>\d+)/$', delete_plant_category, name = 'plant_cat_delete'),
    url(r'^plant/stock/list/$', PlantStockList.as_view(), name = 'plant_stock_list'),
    url(r'^plant/load/update/(?P<id>\d+)/$', load_plant_update_form, name = 'load_plant_update_form'),
    url(r'^plant/update/(?P<id>\d+)/$', update_plant, name = 'update_plant'),
    url(r'^plant/stock/load/update/(?P<id>\d+)/$', update_plantstock_loadform, name = 'update_plantstock_loadform'),
    url(r'^plant/stock/update/(?P<id>\d+)/$', update_plantstock, name = 'update_plantstock'),

    # pesticide urls
    url(r'^pesticide/list/$', PesticideList.as_view(), name = 'pesticide_list'),
    url(r'^pesticide/create/$', PesticideCreate.as_view(), name = 'pesticide_create'),
    url(r'^pesticide/delete/(?P<id>\d+)/$', delete_pesticide, name = 'pesticide_delete'),
    url(r'^pesticide/category/$', PesticideCategoryList.as_view(), name = 'pesticide_cat_list'),
    url(r'^pesticide/category/delete/(?P<id>\d+)/$', delete_pesticide_category, name = 'pesticide_cat_delete'),
    url(r'^pesticide/category/create/$', PesticideCatCreate.as_view(), name = 'pesticide_cat_create'),
    url(r'^pesticide/stock/list/$', PesticideStockList.as_view(), name = 'pesticide_stock_list'),
    url(r'^pesticide/load/update/(?P<id>\d+)/$', load_pesticide_update_form, name = 'load_pesticide_update_form'),
    url(r'^pesticide/update/(?P<id>\d+)/$', update_pesticide, name = 'update_pesticide'),
    url(r'^pesticide/stock/load/update/(?P<id>\d+)/$', load_pesticidestock_form, name = 'load_pesticidestock_form'),
    url(r'^pesticide/stock/update/(?P<id>\d+)/$', update_pesticidestock, name = 'update_pesticidestock'),
    # maintenance,schedule,report 
    url(r'^maintenance/list/$', MaintenanceList.as_view(), name = 'maintenance_list'),
    url(r'^maintenance/create/$', MaintenanceCreate.as_view(), name = 'maintenance_create'),
    url(r'^report/$', report, name = 'report'),
    url(r'^sheduling/$', SchedulingList.as_view(), name = 'sheduling'),
    url(r'^schedule/create/$', ScheduleCreate.as_view(), name = 'schedule_create'),
    url(r'^schedule/delete/(?P<id>\d+)/$', delete_schedule, name = 'delete_schedule'),
    url(r'^schedule/load/update/(?P<id>\d+)/$', schedule_updateload_form, name = 'schedule_updateload_form'),
    url(r'^schedule/update/(?P<id>\d+)/$', schedule_update, name = 'schedule_update'),



    


    





]
