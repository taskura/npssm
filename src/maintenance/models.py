from django.db import models

# Create your models here.
from plant.models import PlantInfo,PlantStock
from pesticide.models import pesticideInfo

class Maintenance(models.Model):
	plant_name = models.ForeignKey(PlantInfo,on_delete=models.CASCADE,related_name="mplant_name")
	plant_quantity = models.PositiveIntegerField(default=0)
	pesticide_name = models.ForeignKey(pesticideInfo,on_delete=models.CASCADE,related_name="mpesticide_name")
	pesticide_quantity = models.PositiveIntegerField(default=0)
	timestemp = models.DateField()

	def __str__(self):
		return str(self.id)


SCHEDULE_DAYS = (
	("1","1 Days"),
	("2","2 Days"),
	("5","5 Days"),
	("7","7 Days"),
	("10","10 Days")
	)


from django.utils import timezone

def get_current_date():
    return timezone.localtime(timezone.now()).date()

class Sheduling(models.Model):
	plant = models.ForeignKey(PlantInfo,on_delete=models.CASCADE,related_name="nplant_name")
	pesticide = models.ForeignKey(pesticideInfo,on_delete=models.CASCADE,related_name="npesticide_name")
	schedule_days = models.CharField(max_length = 100, choices=SCHEDULE_DAYS,default="7")
	timestemp = models.DateField(default=get_current_date, blank=True)
	def __str__(self):
		return str(self.id)
