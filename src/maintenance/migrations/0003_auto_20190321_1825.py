# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-03-21 18:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('maintenance', '0002_auto_20190321_1822'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maintenance',
            name='pesticide_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mpesticide_name', to='pesticide.pesticideInfo'),
        ),
        migrations.AlterField(
            model_name='maintenance',
            name='plant_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mplant_name', to='plant.PlantInfo'),
        ),
    ]
