# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-04-19 18:48
from __future__ import unicode_literals

from django.db import migrations, models
import maintenance.models


class Migration(migrations.Migration):

    dependencies = [
        ('maintenance', '0007_maintenance_timestemp'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sheduling',
            name='timestemp',
            field=models.DateField(blank=True, default=maintenance.models.get_current_date),
        ),
    ]
