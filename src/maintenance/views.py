from django.shortcuts import render,redirect
from django.template.loader import render_to_string
from django.views.generic import ListView,CreateView
from .models import Maintenance,Sheduling
from .forms import MaintenanceForm,SchedulingForm
from pesticide.models import PesticideStock
from django.http import JsonResponse
from twilio.rest import Client
from django.core.mail import send_mail


# Create your views here.
def home(request):
	context = {}
	return render(request,'home.html',context)

class MaintenanceList(ListView):
	model = Maintenance
	template_name = 'maintenance/maintenance_list.html'
	context_object_name = 'maintenence_obj'
	ordering = "-pk"

	def get_context_data(self,**kwargs):
		data = super(MaintenanceList,self).get_context_data(**kwargs)
		data["maintenance_form"] = MaintenanceForm()
		return data

class MaintenanceCreate(CreateView):
	model = Maintenance
	template_name = 'maintenance/maintenance_create.html'
	form_class = MaintenanceForm
	success_url = '/maintenance/list/'

	def form_valid(self,form,*args,**kwargs):
		self.object = form.save()
		pesticide_qty = self.object.pesticide_quantity
		pesticide_obj = PesticideStock.objects.get(pesticide_name = self.object.pesticide_name)

		pesticide_obj.pesticide_quantity = int(pesticide_obj.pesticide_quantity) - int(pesticide_qty)
		print(pesticide_obj.pesticide_quantity)

		if pesticide_obj.pesticide_quantity <= 5:
			custom_message = "{} Quantity is less then 5kg".format(pesticide_obj.pesticide_name)
			account_sid = "AC7ec106fbc0d05f6cea806c416e1ce192"
			auth_token = "c62bf62bc788dcda4257f45bcda597cc"
			client = Client(account_sid, auth_token)
			client.messages.create(  
				to="+8801863714903",  
				from_="+15129942395",  
				body=custom_message,  
				)

			send_mail(
				    'Nursery',
				    custom_message,
				    'info@nursery.com',
				    ['admin@nursery.com'],
				)
			pesticide_obj.save()

		else:
			pesticide_obj.save()

			print("quantity updated succcessfully")


		return super(MaintenanceCreate,self).form_valid(form)



class SchedulingList(ListView):
	model = Sheduling
	template_name = 'maintenance/sheduling.html'
	context_object_name = 'scheduling_obj'
	ordering = "-pk"


	def get_context_data(self,**kwargs):
		data = super(SchedulingList,self).get_context_data(**kwargs)
		data["schedule_form"] = SchedulingForm()
		return data

class ScheduleCreate(CreateView):
	model = Sheduling
	template_name = 'maintenance/schedule_create.html'
	form_class = SchedulingForm
	success_url = '/sheduling/'

def delete_schedule(request,id):
    schedule = Sheduling.objects.get(id=id)
    schedule.delete()
    return redirect("/sheduling/")


def schedule_updateload_form(request,id):
	data = dict()
	schedule_obj = Sheduling.objects.get(id=id)
	form = SchedulingForm(instance=schedule_obj)
	context = {
		'schedule_form':form
	}
	update_html = render_to_string('maintenance/schedule_update.html',context,request=request)
	data['schedule_form'] = update_html
	return JsonResponse(data)

def schedule_update(request,id):
	schedule_obj = Sheduling.objects.get(id=id)
	if request.method == 'POST':
		form = SchedulingForm(request.POST,instance=schedule_obj)
		if form.is_valid():
			form.save()
	return redirect("/sheduling/")


def report(request):
	from_date = ""
	to_date = ""
	final_report = []
	total_entity = ""
	if request.method=='POST':
		from_date = request.POST.get('from_date')
		to_date = request.POST.get('to_date')
		report_data = Maintenance.objects.filter(timestemp__gte=from_date)
		final_report = report_data.filter(timestemp__lte=to_date)
		total_entity = final_report.count()

	context = {
		"final_report":final_report,
		"from_date":from_date,
		"to_date":to_date,
		"total_entity":total_entity



	}
	return render(request,'maintenance/report.html',context)