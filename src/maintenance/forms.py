from django.forms import ModelForm, inlineformset_factory, TextInput
from .models import Maintenance,Sheduling




class MaintenanceForm(ModelForm):
	class Meta:
		model = Maintenance
		fields = '__all__'



class SchedulingForm(ModelForm):
	class Meta:
		model = Sheduling
		exclude = ['timestemp']