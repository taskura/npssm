$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
    });
    $('.side-nav .collapse').on("show.bs.collapse", function() {                        
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
    });
})    

$(document).ready(function(){

});

function updatePlant(instance){
	var url = $(instance).attr('data_url')
    $.ajax({
      url: url,
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#updatePlantForm").html("");
      },
      success: function (data) {
        $("#updatePlantForm").html(data.plant_form);
        $("#plantUpdateModal").modal("show");
      }

      });

}

function updatePesticide(instance){
  var url = $(instance).attr('data_url')
    $.ajax({
      url:url,
      type: 'get',
      dataType: 'json',
      beforeSend: function(){
        $("#updatePesticideForm").html("");
      },
      success: function(data){
        $("#updatePesticideForm").html(data.pesticide_form);
        $("#pesticideUpdateModal").modal("show")
      }
    });
}

function updatePlantStock(instance){
  var url = $(instance).attr('data_url')
  $.ajax({
    url:url,
    type:'get',
    dataType:'json',
    beforeSend:function(){
      $("#StockLoadForm").html("");
    },
    success: function(data){
      $("#StockLoadForm").html(data.stock_form);
      $("#plantStockUpdateModal").modal("show");

    }

  })
}

function updatePesticideStock(instance){
  var url = $(instance).attr('data_url')
  $.ajax({
    url:url,
    type:'get',
    dataType:'json',
    beforeSend:function(){
      $("#stockloadform").html("");
    },
    success: function(data){
      $("#stockloadform").html(data.stock_form);
      $("#pesticideStockUpdateModal").modal("show");

    }

  })
}

function updateSchedule(instance){
  var url = $(instance).attr('data_url')
  $.ajax({
    url:url,
    type:'get',
    dataType:'json',
    beforeSend:function(){
      $('#updateScheduleForm').html("");
    },
    success:function(data){
      $('#updateScheduleForm').html(data.schedule_form);
      $('#ScheduleUpdateModal').modal('show');
    }
  })
}